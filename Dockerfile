# Add LLVM and Numba into the standard FEniCS container

FROM fenicsproject/stable:latest

USER root
RUN sudo apt-get update && \
    sudo apt-get install -y llvm-3.6 libedit-dev && \
    sudo pip install enum34 && \
    sudo LLVM_CONFIG=/usr/bin/llvm-config-3.6 pip install llvmlite numba && \
    sudo apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
