from numba.pycc import CC

cc = CC('my_module')

@cc.export('multf', 'f8(f8, f8)')
def mult(a, b):
    return a*b

cc.compile()
