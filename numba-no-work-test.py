import numpy as np
import numba
from numba import jit as numba_jit
from numpy import arange

@numba_jit(nopython=True)
def sum2d(arr):
    M, N = arr.shape
    result = 0.0
    for i in range(M):
        for j in range(N):
            result += arr[i, j]
    return result

a = arange(9).reshape(3, 3)
print a
print sum2d(a)

from dolfin import *

import types
print types.FunctionType

code = '''
class MyFunc : public Expression
{
public:

    std::shared_ptr<MeshFunction<std::size_t> > cell_data;

    MyFunc() : Expression()
    {
    }

    void eval(Array<double>& values, const Array<double>& x,
    const ufc::cell& c) const
    {
        assert(cell_data);
        values[0] = 0.0;
    }
};'''

class NumbaExpression(Expression):
    @numba_jit(cache=True)
    def eval(self, value, x):
        value[0] = 0.0 

class PythonExpression(Expression):
    def eval(self, value, x):
        value[0] = 0.0

numba_exp = NumbaExpression()
python_exp = PythonExpression()
cpp_exp = Expression(code)

print NumbaExpression.eval.inspect_types()

mesh = UnitSquareMesh(500, 500)
V = FunctionSpace(mesh, "CG", 1)

t = Timer("Z Numba Interpolation...")
exp_V = interpolate(numba_exp, V)
print exp_V.vector().array()
del(t)

t = Timer("Z C++ Interpolation...")
exp_V = interpolate(cpp_exp, V)
print exp_V.vector().array()
del(t)

t = Timer("Z Python Interpolation...")
exp_V = interpolate(python_exp, V)
print exp_V.vector().array()
del(t)

# TODO: Use assert_almost_equal

list_timings()
